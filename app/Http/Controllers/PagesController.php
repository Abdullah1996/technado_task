<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    //
    public function index(){
        return view('upload');
    }
    public function submit(Request $req){
        $file = $req->file('image');
        $extension = $file->getClientOriginalExtension();
        $filename = time() . '.' . $extension; //change file name for short
        $file->move('images', $filename);

        return 'Uploaded Successfully! ... FILE PATH \public\images';


    }
}