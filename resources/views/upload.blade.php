@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Upload Image') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{url('/submit_image')}}" enctype="multipart/form-data">
                        @csrf
                        <input type="file" name="image" accept=".jpeg">
                        <input type="submit" value="Submit">
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection